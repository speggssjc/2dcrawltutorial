﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public Slider healthBar;
	public Text HPText;
	public PlayerHealthManager playerHealth;
	
	public Slider expBar;
	public Text XPText;
	public Text levelText;
	private PlayerStats thePS;
	
	private static bool UIExists;

	void Start () {
		if (!UIExists) {
			UIExists = true;
			DontDestroyOnLoad (transform.gameObject);
		} else {
			Destroy (gameObject);
		}
		
		thePS = GetComponent<PlayerStats>();
	}
	
	void Update () {
		healthBar.maxValue = playerHealth.playerMaxHealth;
		healthBar.value = playerHealth.playerCurrentHealth;
		HPText.text = "HP: " + playerHealth.playerCurrentHealth + "/" + playerHealth.playerMaxHealth;
		levelText.text = "Lvl: " + thePS.currentLevel;
		
		int maxXP = thePS.toLevelUp[thePS.currentLevel];
		expBar.maxValue = maxXP;
		expBar.value = thePS.currentExp;
		XPText.text = "XP: " + thePS.currentExp + "/" + maxXP; //thePS.toLevelUp[thePS.currentLevel];
	}
}
