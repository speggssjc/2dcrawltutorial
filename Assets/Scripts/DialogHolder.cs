﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogHolder : MonoBehaviour {

	public string dialogue;
	private DialogueManager dMan;
	private TouchControls theTC;
	
	public string[] dialogueLines;
	
	void Start () {
		dMan = FindObjectOfType<DialogueManager>();
		theTC = FindObjectOfType<TouchControls>();
	}
	
	void Update () {
		
	}
	
	void OnTriggerStay2D(Collider2D other){
		if(other.gameObject.name == "Player"){
			if(Input.GetKeyUp(KeyCode.Space) || theTC.speakUp){
				theTC.speakUp = false;
				//dMan.ShowBox(dialogue);
				if(!dMan.dialogActive){
					dMan.dialogLines = dialogueLines;
					dMan.currentLine = -1;
					dMan.ShowDialogue();
				}
				
				if(transform.parent.GetComponent<VillagerMovement>() != null){
					transform.parent.GetComponent<VillagerMovement>().canMove = false;
				}
			}
		}
	}
}
